from pytest import fixture
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


@fixture(scope='session')  # function = different browsers, session = 1 browser
def driver():
    options = Options()
    options.headless = False
    driver = webdriver.Chrome(chrome_options=options, executable_path=r'D:\Python\webdrivers\chromedriver.exe')
    driver.maximize_window()
    yield driver
    driver.quit()



"""
Start test options:
pytest -m fixture name  -  starts all test with a certain fixture assigned to functions
pytest -v  -  starts all tests within test suite
pytest -m fixture name --html="report example name.html"  -  starts test with generated html report
pytest -v -s --html="report example name.html" file_inlay_name.py - starts all test within one python file 

"""
