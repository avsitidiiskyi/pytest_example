from pytest import mark

@mark.smoke
@mark.engine
@mark.ui
def test_engine_is_ok(driver):
    try:
        driver.get("https://en.wikipedia.org/wiki/Engine")
        driver.maximize_window()
        assert "thermodynamic processes" in driver.page_source
    finally:
        pass


