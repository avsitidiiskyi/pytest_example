from pytest import mark
from time import sleep
more_than_100 = r"D:\Test Data\PDFs\MORE than 100.pdf"
protected = r"D:\Test Data\PDFs\secured(password = password).pdf"


class SplitTests:

    @mark.limit
    def test_ddm_en_size(self, driver):
        driver.get("https://www.sodapdf.com/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "The file size exceeds the maximum allowed size" in driver.page_source

    @mark.limit
    def test_ddm_fr_size(self, driver):
        driver.get("https://www.sodapdf.com/fr/diviser-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "La taille du fichier dépasse la taille maximale autorisée" in driver.page_source

    @mark.limit
    def test_ddm_de_size(self, driver):
        driver.get("https://www.sodapdf.com/de/pdf-teilen/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "Die Dateigröße überschreitet die maximal zulässige Größe" in driver.page_source

    @mark.limit
    def test_ddm_it_size(self, driver):
        driver.get("https://www.sodapdf.com/it/dividere-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "La dimensione del file supera le dimensioni massime consentite" in driver.page_source

    @mark.limit
    def test_ddm_es_size(self, driver):
        driver.get("https://www.sodapdf.com/es/dividir-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "El tamaño del archivo supera el tamaño máximo permitido" in driver.page_source

    @mark.limit
    def test_ddm_pt_size(self, driver):
        driver.get("https://www.sodapdf.com/pt/dividir-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "O tamanho do arquivo excede o tamanho máximo permitido" in driver.page_source

    @mark.limit
    def test_ddm_ru_size(self, driver):
        driver.get("https://www.sodapdf.com/ru/%D1%80%D0%B0%D0%B7%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-"
                   "%D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%B2-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "Размер файла превышает максимально допустимый размер" in driver.page_source

    @mark.limit
    def test_ddm_sv_size(self, driver):
        driver.get("https://www.sodapdf.com/sv/dela-upp-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "Filstorlek överstiger maximalt tillåten storlek" in driver.page_source

    @mark.limit
    def test_ddm_vi_size(self, driver):
        driver.get("https://www.sodapdf.com/vi/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "Kích thước tập tin vượt quá kích thước tối đa được phép" in driver.page_source

    @mark.limit
    def test_ddm_id_size(self, driver):
        driver.get("https://www.sodapdf.com/id/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "Ukuran file melebihi ukuran maksimum yang diizinkan" in driver.page_source

    @mark.limit
    def test_ddm_ja_size(self, driver):
        driver.get("https://www.sodapdf.com/ja/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "この種のファイルはサポートされていません" in driver.page_source

    @mark.limit
    def test_ddm_ko_size(self, driver):
        driver.get("https://www.sodapdf.com/ko/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "파일 크기가 최대 허용 크기를 초과했습니다" in driver.page_source

    @mark.limit
    def test_ddm_pl_size(self, driver):
        driver.get("https://www.sodapdf.com/pl/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "Rozmiar pliku przekracza maksymalny dozwolony rozmiar" in driver.page_source

    @mark.limit
    def test_ddm_tr_size(self, driver):
        driver.get("https://www.sodapdf.com/tr/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(more_than_100)
        sleep(2)
        assert "Dosya izin verilen maksimum boyutu aşıyor" in driver.page_source

    """
    password protected
    """

    @mark.protected
    def test_ddm_en_protect(self, driver):
        driver.get("https://www.sodapdf.com/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(8)
        assert "The uploaded file is password protected and cannot be split" in driver.page_source

    @mark.protected
    def test_ddm_fr_protect(self, driver):
        driver.get("https://www.sodapdf.com/fr/diviser-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "Le fichier chargé est protégé par un mot de passe et ne peut pas être divisé" in driver.page_source

    @mark.protected
    def test_ddm_de_protect(self, driver):
        driver.get("https://www.sodapdf.com/de/pdf-teilen/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "Die hochgeladene Datei ist passwortgeschützt und kann nicht geteilt werden" in driver.page_source

    @mark.protected
    def test_ddm_it_protect(self, driver):
        driver.get("https://www.sodapdf.com/it/dividere-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "Il file caricato è protetto da password e non può essere diviso" in driver.page_source

    @mark.protected
    def test_ddm_es_protect(self, driver):
        driver.get("https://www.sodapdf.com/es/dividir-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "El archivo cargado está protegido con contraseña y no se puede dividir" in driver.page_source

    @mark.protected
    def test_ddm_pt_protect(self, driver):
        driver.get("https://www.sodapdf.com/pt/dividir-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "O arquivo carregado está protegido com senha e não pode ser dividido" in driver.page_source

    @mark.protected
    def test_ddm_ru_protect(self, driver):
        driver.get("https://www.sodapdf.com/ru/%D1%80%D0%B0%D0%B7%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-"
                   "%D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%B2-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "Загруженный файл защищен паролем и его нельзя разделить" in driver.page_source

    @mark.protected
    def test_ddm_sv_protect(self, driver):
        driver.get("https://www.sodapdf.com/sv/dela-upp-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "Den uppladdade filen är lösenordsskyddad och kan inte delas upp" in driver.page_source

    @mark.protected
    def test_ddm_vi_protect(self, driver):
        driver.get("https://www.sodapdf.com/vi/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "Tập tin được tải lên được bảo vệ bằng mật khẩu và không thể được tách" in driver.page_source

    @mark.protected
    def test_ddm_id_protect(self, driver):
        driver.get("https://www.sodapdf.com/id/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "File yang diunggah terlindungi sandi dan tidak dapat dipisahkan" in driver.page_source

    @mark.protected
    def test_ddm_ja_protect(self, driver):
        driver.get("https://www.sodapdf.com/ja/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "アップロードされたファイルはパスワードで保護されているため分割できません" in driver.page_source

    @mark.protected
    def test_ddm_ko_protect(self, driver):
        driver.get("https://www.sodapdf.com/ko/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "업로드된 파일은 비밀번호로 보호되며 분할될 수 없습니다" in driver.page_source

    @mark.protected
    def test_ddm_pl_protect(self, driver):
        driver.get("https://www.sodapdf.com/pl/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "Przesłany plik jest chroniony hasłem i nie można go podzielić" in driver.page_source

    @mark.protected
    def test_ddm_tr_protect(self, driver):
        driver.get("https://www.sodapdf.com/tr/split-pdf/")
        sleep(5)
        choose_file = driver.find_element_by_xpath("//input[@type = 'file']")
        choose_file.send_keys(protected)
        sleep(2)
        assert "Yüklenen dosya şifre korumalı ve bölünemiyor" in driver.page_source
